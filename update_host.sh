#!/bin/bash

### This script provides replaces the host variables for BA and SM in importmaps.json to the provided host.
###     
###     Run command: bash update_host.sh ${HOST}
###     example: bash update_host.sh https://sample.org
### 

### Set Colors
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
RESET=`tput sgr0`

### Escape special character /
HOST=$1
HOST=$(sed 's/\//\\\//g' <<< $HOST)

BA_HOST_URL="${HOST}\/mfe\/ba\/dev\/ebs-ba.js"
SM_HOST_URL="${HOST}\/mfe\/sm\/dev\/ebs-sm.js"

BA_HOST_STR="\"@ebs/ba\":"
SM_HOST_STR="\"@ebs/sm\":"

FILE="importmaps.json"

if [ $# -eq 0 ]; then
    echo;
    echo "$RED ☒  Invalid command. Please provide host. $RESET"
    exit;
fi

while read -r line; do
    ### BA
    if [[ "$line" == *"${BA_HOST_STR}"* ]] ;then
        ### Escape special characters
        BA_HOST_STR=$(sed 's/\//\\\//g' <<< $BA_HOST_STR)
        
        if sed -i -e "s/${BA_HOST_STR}.*/${BA_HOST_STR} \"${BA_HOST_URL}\",/" $FILE; then
            ### Remove escape characters
            BA_HOST_URL=$(sed 's/\\//g' <<< $BA_HOST_URL)        
            echo;
            echo "$GREEN ☒  Host successfully updated to $BA_HOST_URL. $RESET"
        else 
            echo;
            echo "$RED ☒  Update of host failed. Kindly check for unescaped special characters. $RESET"
            exit;
        fi
    fi

    ### SM
    if [[ "$line" == *"${SM_HOST_STR}"* ]] ;then

        ### Escape special characters
        SM_HOST_STR=$(sed 's/\//\\\//g' <<< $SM_HOST_STR)

        if sed -i -e "s/${SM_HOST_STR}.*/${SM_HOST_STR} \"${SM_HOST_URL}\"/" $FILE; then
            ### Remove escape characters
            SM_HOST_URL=$(sed 's/\\//g' <<< $SM_HOST_URL)
            echo;
            echo "$GREEN ☒  Host successfully updated to $SM_HOST_URL. $RESET"
            exit;
        else 
            echo;
            echo "$RED ☒  Update of host failed. Kindly check for unescaped special characters. $RESET"
            exit;
        fi
    fi

done < "$FILE"